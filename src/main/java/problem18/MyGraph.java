package problem18;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyGraph<V> extends UnweightedGraph<V> {
    public MyGraph() {
    }

    public MyGraph(V[] vertices, int[][] edges) {
        super(vertices, edges);
    }

    public MyGraph(List<V> vertices, List<Edge> edges) {
        super(vertices, edges);
    }

    public MyGraph(List<Edge> edges, int numberOfVertices) {
        super(edges, numberOfVertices);
    }

    public MyGraph(int[][] edges, int numberOfVertices) {
        super(edges, numberOfVertices);
    }

    public List<List<Integer>> getConnectedComponents() {
        List<List<Integer>> result = new ArrayList<>();
        Set<Integer> vertices = new HashSet<>(getVertices().size());

        for (int i = 0; i < getVertices().size(); i++) {
            vertices.add(i);
        }

        while (!vertices.isEmpty()) {
            Set<Integer> group = getConnectedVertices(vertices.iterator().next(),
                    new HashSet<Integer>());
            result.add(new ArrayList<>(group));
            vertices.removeAll(group);
        }
        return result;
    }

    private Set<Integer> getConnectedVertices(int vertexIndex, Set<Integer> currentGroup) {
        currentGroup.add(vertexIndex);
        currentGroup.addAll(getNeighbors(vertexIndex));
        Set<Integer> neighbors = new HashSet<>(getNeighbors(vertexIndex));
        neighbors.removeAll(currentGroup);

        for (Integer neighbor : neighbors) {
            currentGroup.addAll(getConnectedVertices(neighbor, currentGroup));
        }

        return currentGroup;
    }

}
