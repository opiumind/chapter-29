package problem18;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by Olga Lisovskaya on 12/10/17.
 */
public class WeightedGraphTest extends TestCase {
    public void testGetShortestPathAlternative() throws Exception {
        int[][] edges = new int[][] {
                {0, 1, 2}, {0, 3, 8},
                {1, 0, 2}, {1, 2, 7}, {1, 3, 3},
                {2, 1, 7}, {2, 3, 4}, {2, 4, 5},
                {3, 0, 8}, {3, 1, 3}, {3, 2, 4}, {3, 4, 6},
                {4, 2, 5}, {4, 3, 6}
        };
        WeightedGraph<Integer> graph = new WeightedGraph<>(edges, 5);
        WeightedGraph<Integer>.ShortestPathTree tree =
                graph.getShortestPath(3);
        Assert.assertEquals("Wrong shortest path tree.", "All shortest paths from 3 are:A path from 3 to 0: 3 1 0 (cost: 5.0)A path from 3 to 1: 3 1 (cost: 3.0)A path from 3 to 2: 3 2 (cost: 4.0)A path from 3 to 3: 3 (cost: 0.0)A path from 3 to 4: 3 4 (cost: 6.0)", tree.printAllPathsToString());
    }

}